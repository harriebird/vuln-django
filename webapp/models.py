from django.db import models

# Create your models here.


class Account(models.Model):
    username = models.CharField(max_length=10)
    password = models.CharField(max_length=30)
    fullname = models.CharField(max_length=50)
