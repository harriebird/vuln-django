from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib import messages
from .models import Account


# Create your views here.
# from django.shortcuts import get_object_or_404, render
def index(request):
    if 'user_id' in request.session.keys() and request.session['user_id']:
        return redirect('home')
    messages.error(request, '귀하의 계정에 로그인하십시오.')
    response = render(request, 'webapp/index.html')
    response.delete_cookie('ctf_key')
    return response


def logout(request):
    try:
        del request.session['user_id']
    except KeyError:
        pass
    messages.success(request, '당신은 성공적으로 로그 아웃했습니다.')
    return redirect('index')


def home(request):
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        account = Account.objects.raw("SELECT * FROM webapp_account WHERE username = '{}' AND password = '{}'"
                                      .format(username, password))
        if len(account) > 0:
            if 'HTTP_OPERATION' in request.META.keys() and request.META.get('HTTP_OPERATION') == 'get_key':
                response = JsonResponse({'message': '코드는: HM3CTF{#x4#br)$&_ss5$itepnxz(zk0bqil(01%b0l_2t=2d4=vz04en}'})
            else:
                request.session['user_id'] = account[0].id
                response = render(request, 'webapp/home.html')
                response.set_cookie('ctf_key', 'flag{M3_W4NT_BROWSER_COOKI3}')
            return response
        else:
            if 'HTTP_OPERATION' in request.META.keys() and request.META.get('HTTP_OPERATION') == 'get_key':
                return JsonResponse({'error': '사용자 이름 또는 비밀번호가 잘못되었습니다.'}, status=401)
            messages.error(request, '사용자 이름 또는 비밀번호가 잘못되었습니다.')
            return redirect('index')

    if request.session['user_id']:
        return render(request, 'webapp/home.html', {'content': request.session['user_id']})
