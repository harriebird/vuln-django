from django.middleware.common import CommonMiddleware
import requests
from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse


class CheckCountryMiddleware(CommonMiddleware):

    def process_request(self, request):
        if not settings.DEBUG:
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                client_ip = x_forwarded_for.split(',')[0]
            else:
                client_ip = request.META.get('REMOTE_ADDR')
            headers = {'Content-Type': 'application/json'}
            api_response = requests.get('https://geolocation-db.com/json/{}'.format(client_ip), headers=headers).json()

            if not api_response['country_code'] == 'KR':
                if 'HTTP_OPERATION' in request.META.keys() and request.META.get('HTTP_OPERATION') == 'get_key':
                    return JsonResponse({'error': '죄송합니다.이 사이트에 액세스 할 수있는 권한이 없습니다.'})
                else:
                    return render(request, 'webapp/unauthorized.html', status=401)
